fn main() {
	let num_steps = 1000000;
	let mut sum: f64 = 0.0;
	let step: f64 = 1.0f64/num_steps as f64;
	for i in 0..num_steps{
	let x = (i as f64 + 0.5f64) * step;
	sum = sum + 4.0/(1.0 + x*x);
	}
	
	let pi = sum * step;
	println!("pi = {}", pi);
	}
